<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
// route su get
Route::get('/', function () {
    return view('welcome');
    // return "Hello World";
});

Route::get('/hello', function () {
    // return view('welcome');
    return "Hello World";
});

Route::get('/helloh1', function () {
    // return view('welcome');
    return "<h1>Hello World</h1>";
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/user/{id}', function ($id) {
    return "this is user ".$id;
});

Route::get('/users/{id}/{name}', function ($id, $name) {
    return "this is user ".$name." with an id of ".$id;
});
*/

// route per controler
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about')->name('about');
Route::get('/services', 'PagesController@services');

Route::resource('posts', 'PostController');
Auth::routes();

Route::get('/dashboard', 'DashboardController@index');