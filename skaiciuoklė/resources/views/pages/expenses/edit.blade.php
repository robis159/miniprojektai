
<div class="modal fade" id="editExpenses-{{ $expen->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Redaguoti pasirinktą išlaidą</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['expenses.update', $expen->id], 'method' => 'POST']) !!}
                <div class="modal-body">

                    <div class="row m-4">
                        <div class="col-sm-5">
                            {{ Form::label('data', "Iveskite data") }}
                        </div>
                        <div class="col-sm-7">
                            {{Form::date('date', $expen->date, ['class' => 'input-group-text',])}}
                        </div>
                    </div>

                    <div class="row m-4">
                        <div class="col-sm-5">
                            {{ Form::label('name', "Išlaidų pavadinimas") }}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('name', $expen->name, ['class' => 'input-group-text'])}}
                        </div>
                    </div>

                    <div class="row m-4">
                        <div class="col-sm-5">
                            {{ Form::label('expenses', "Išlaidų suma") }}
                        </div>
                        <div class="col-sm-6">
                            {{Form::text('expenses', $expen->expenses, ['class' => 'input-group-text'])}}
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    {{ Form::hidden('_method', 'PUT') }}
                    {{ Form::submit('Uždaryti', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal' ]) }}
                    {{ Form::submit('Pakeisti', ['class' => 'btn btn-primary']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>