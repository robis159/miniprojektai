@extends('layouts.main')

@section('content')

<div class="container">

    <a class="btn btn-success mb-2" data-toggle="modal" data-target="#addExpenses" href="{{ route('expenses.create') }}">Pridėti išlaidas</a>

    {{-- modal --}}
    @include('pages.expenses.create')
    
    {{-- .modal --}}

    <table class="table table-hover table-sm">
        <thead class="thead-dark">
            <tr>
                <th>data</th>
                <th>Pavadinimas</th>
                <th>Suma</th>
                <th>Vartotojas</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @if (count($expenses) > 0)

                @foreach ($expenses as $expen)
                    @include('pages.expenses.edit')
                    <tr>
                    <td>{{$expen->date}}</td>
                    <td>{{$expen->name}}</td>
                    <td>{{$expen->expenses}}</td>
                    <td>{{$expen->user}}</td>
                    <td><a href="/expenses/{{$expen->id}}/edit" data-toggle="modal" data-target="#editExpenses-{{ $expen->id }}" class="btn btn-primary">Redaguoti</a></td>
                    <td>
                        {{Form::open(['action' => ['ExpensesController@destroy', $expen->id], 'method' => 'POST'])}}
                            {{Form::hidden('_method', 'DELETE')}}
                            {{Form::submit('Ištrinti', ['class' => 'btn btn->danger'])}}
                        {{Form::close()}}
                    </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
@endsection
