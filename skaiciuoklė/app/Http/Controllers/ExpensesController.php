<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//iskvieciam modeli
use App\Expenses;
//iskvieciam validator
use Illuminate\Support\Facades\Storage;
use DeepCopy\TypeFilter\ReplaceFilter;


class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expen = Expenses::where('user', auth()->user()->name)->
        orderBy('created_at', 'desc')->get();
        $expenses = Expenses::orderBy('created_at', 'desc')->get();
        return view('pages.expenses.index')->with('expenses', $expen);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.expenses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //padarome kad visa musu ivedama informacija butu privaloma
        $this->validate($request, [
            'date' => 'required',
            'name' => 'required',
            'expenses' => 'required'
        ]);

        //kuriame naujas islaidas
        $exp = new Expenses;
        $exp->date = $request->date;
        $exp->name = $request->name;
        $exp->expenses = str_replace(',', '.', $request->expenses);
        $exp->user = auth()->user()->name;
        $exp->save();

        return redirect(route('expenses.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expen = Expenses::find($id);
        return view('pages.expenses.edit')->with('expen', $expen);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'date' => 'required',
            'name' => 'required',
            'expenses' => 'required'
        ]);


        Expenses::where('id', $id)->update([
            'date' => $request->date,
            'name' => $request->name,
            // 'expenses' => $request->expenses,
            'expenses' => str_replace(',', '.', $request->expenses),
        ]);

        return redirect('/expenses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exp = Expenses::find($id);
        $exp->delete();

        return redirect('/expenses');
    }
}
